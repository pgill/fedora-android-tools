#
# This code is licenced under the GNU GENERAL PUBLIC LICENSE Version 2, June
# 1991.
#
#!/bin/env bash
ROOT_PATH="$( cd "$(dirname "$0")" ; pwd -P )"

BUILD_VERSION="unstable"

CORE_PREFIX="core"
BORINGSSL_PREFIX="boringssl"
MDNSRESPONDER_PREFIX="mdnsresponder"

#SOURCES
SOURCES_PATH="$ROOT_PATH/sources"
CORE_SOURCE_PATH="$SOURCES_PATH/core"
BORINGSSL_SOURCE_PATH="$SOURCES_PATH/boringssl"
MDNSRESPONDER_SOURCE_PATH="$SOURCES_PATH/mdnsresponder"
FEDORA_SPEC_SOURCE_PATH="$SOURCES_PATH/fedora-android-tools"
FEDORA_SPEC_BRANCH="master"

CORE_TAR="$SOURCES_PATH/core.tar.xz"
BORINGSSL_TAR="$SOURCES_PATH/boringssl.tar.xz"
MDNSRESPONDER_TAR="$SOURCES_PATH/mdnsresponder.tar.xz"

#BUILD OUTPUT
BUILDROOT_PATH="$ROOT_PATH/result"
CORE_PATH="$BUILDROOT_PATH/$CORE_PREFIX"
BORINGSSL_PATH="$BUILDROOT_PATH/$BORINGSSL_PREFIX"
MDNSRESPONDER_PATH="$BUILDROOT_PATH/$MDNSRESPONDER_PREFIX"

BUILD_SCRIPT_PATH="$BUILDROOT_PATH/build.ninja"

function delete_file() {
	if [ -f "$1" ]
	then
		rm -rf "$1"
	fi
}

function delete_directory() {
	if [ -d "$1" ]
	then
		rm -rf "$1"
	fi
}

function checkout_sources() {
	delete_directory "$SOURCES_PATH"
	mkdir "$SOURCES_PATH"
	pushd "$SOURCES_PATH" >> /dev/null
	git clone https://android.googlesource.com/platform/system/core.git "$CORE_SOURCE_PATH"
	git clone https://android.googlesource.com/platform/external/boringssl "$BORINGSSL_SOURCE_PATH"
	git clone https://android.googlesource.com/platform/external/mdnsresponder "$MDNSRESPONDER_SOURCE_PATH"
	git clone -b "$FEDORA_SPEC_BRANCH" https://src.fedoraproject.org/git/rpms/android-tools.git "$FEDORA_SPEC_SOURCE_PATH"
	popd >> /dev/null
}

function archive_repo() {

	pushd "$1" >> /dev/null
	git archive --format=tar --prefix="$2/" "$3" $4 | xz  > "$5"
	popd >> /dev/null

}

function create_tars() {

	CORE_COMMIT=$(grep "%global git_commit" "$FEDORA_SPEC_SOURCE_PATH/android-tools.spec" | awk '{print $3}')
	BORINGSSL_COMMIT=$(grep "%global boring_git_commit" "$FEDORA_SPEC_SOURCE_PATH/android-tools.spec" | awk '{print $3}')
	MDNSRESPONDER_COMMIT=$(grep "%global mdns_git_commit" "$FEDORA_SPEC_SOURCE_PATH/android-tools.spec" | awk '{print $3}')

	BUILD_VERSION="$CORE_COMMIT"

	rm -f $CORE_TAR $BORINGSSL_TAR $MDNSRESPONDER_TAR

	archive_repo \
		"$CORE_SOURCE_PATH" \
		"$CORE_PREFIX/" \
		"$CORE_COMMIT" \
		"adb base diagnose_usb fastboot libcrypto_utils libcutils liblog libsparse libsystem libutils libziparchive mkbootimg include" \
		"$CORE_TAR"

	archive_repo \
		"$BORINGSSL_SOURCE_PATH" \
		"$BORINGSSL_PREFIX/" \
		"$BORINGSSL_COMMIT" \
		"src/crypto include src/include" \
		"$BORINGSSL_TAR"

	archive_repo \
		"$MDNSRESPONDER_SOURCE_PATH" \
		"$MDNSRESPONDER_PREFIX/" \
		"$MDNSRESPONDER_COMMIT" \
		"mDNSShared" \
		"$MDNSRESPONDER_TAR"
}

function extract_tars() {
	delete_directory $CORE_PATH
	delete_directory $BORINGSSL_PATH
	delete_directory $CORE_PAMDNSRESPONDER_PATHTH

	pushd "$BUILDROOT_PATH" >> /dev/null
	xz -dc "$CORE_TAR" | tar -xf -
	xz -dc "$BORINGSSL_TAR" | tar -xf -
	xz -dc "$MDNSRESPONDER_TAR" | tar -xf -
	popd >> /dev/null
}

function apply_patch() {
	pushd "$1" >> /dev/null
	patch --no-backup-if-mismatch -p1 -N --fuzz=0 -i "$2" >> /dev/null
	popd >> /dev/null
}

function apply_patches() {
	pushd "$FEDORA_SPEC_SOURCE_PATH" >> /dev/null
	PATCH_FILES=$(ls *.patch)

	while read PATCH_FILE; do
		echo Patching $PATCH_FILE
		PATH_FILE_PATH="$FEDORA_SPEC_SOURCE_PATH/$PATCH_FILE"
		apply_patch "$CORE_PATH" "$PATH_FILE_PATH"
	done <<< "$PATCH_FILES"

	popd >> /dev/null
}

function prepare_build() {

	pushd "$CORE_PATH" >> /dev/null
	sed -i "s/android::build::GetBuildNumber().c_str()/\"$BUILD_VERSION\"/g" adb/adb.cpp
	popd >> /dev/null

	delete_file "$BUILD_SCRIPT_PATH"
	export CC="clang"
	export CXX="clang++"
	export CFLAGS="-g"
	export CXXFLAGS="-g -fno-elide-constructors"
	ruby "$FEDORA_SPEC_SOURCE_PATH/generate_build.rb" >> "$BUILD_SCRIPT_PATH"
}

function install_dependencies() {
	DEPENDENCIES="clang ninja-build zlib-devel openssl-devel libselinux-devel gtest-devel libusbx-devel systemd ruby rubygems"
	sudo dnf install $DEPENDENCIES
}

function do_build() {
	pushd "$BUILDROOT_PATH" >> /dev/null
	ninja
	popd >> /dev/null
}

function main() {
	delete_directory "$SOURCES_PATH"
	mkdir "$SOURCES_PATH"
	checkout_sources
	create_tars

	delete_directory "$BUILDROOT_PATH"
	mkdir "$BUILDROOT_PATH"
	extract_tars
	apply_patches
	prepare_build
	#install_dependencies
	do_build
}

main